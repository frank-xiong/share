package com.frank.practice.template;

public class Utils {

    // 打印一个数组
    public static <T> void showArray(T[] array) {
        if (array == null) {
            System.out.println("-> array is null");
        } else if (0 == array.length) {
            System.out.println("-> array is empty");
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("-> [");
            sb.append(array[0]);
            for (int i = 1; i < array.length; ++i) {
                sb.append(", ").append(array[i]);
            }
            sb.append("]");
            System.out.println(sb);
        }
    }

    // 打印一个数组
    public static void showArray(int[] array) {
        if (array == null) {
            System.out.println("-> array is null");
        } else if (0 == array.length) {
            System.out.println("-> array is empty");
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("-> [");
            sb.append(array[0]);
            for (int i = 1; i < array.length; ++i) {
                sb.append(", ").append(array[i]);
            }
            sb.append("]");
            System.out.println(sb);
        }
    }
}
