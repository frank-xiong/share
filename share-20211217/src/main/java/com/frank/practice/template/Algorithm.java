package com.frank.practice.template;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Algorithm {

    @FunctionalInterface
    public interface Comparator<T> {
        boolean lessThan(T a, T b);
    }

    // 一个简单的插入排序
    public static <T> void sort(T[] array, Comparator<T> comparator) {
        for (int i = 1; i < array.length; ++i) {
            T key = array[i];
            int j = i;
            if (comparator.lessThan(key, array[0])) {
                for (; 0 < j; --j) {
                    array[j] = array[j - 1];
                }
            } else {
                for (; comparator.lessThan(key, array[j - 1]); --j) {
                    array[j] = array[j - 1];
                }
            }

            array[j] = key;
        }
    }

    public static <T extends Comparable<T>> void sort(T[] array) {
        sort(array, (a, b)->{ return a.compareTo(b) < 0; });
    }

    @FunctionalInterface
    public interface ApplyList<T> {
        boolean accept(List<T> list);
    }

    // 排列算法，注意 source 元素不能重复
    public static <T> int permutation(List<T> source, ApplyList<T> apply) {
        if (source == null || source.isEmpty()) {
            return 0;
        }

        return permutation(source, source.size(), apply);
    }

    public static <T> int permutation(List<T> source, int n, ApplyList<T> apply) {
        if (source == null || source.isEmpty() || n < 1) {
            return 0;
        }
        if (source.size() < n) {
            n = source.size();
        }

        int count = 0;
        Stack<Integer> stack = new Stack<>();
        for (int i = 0; ; ) {
            Integer index = findIndexInStack(stack, i, source.size());
            if (index == null) {
                if (stack.empty()) {
                    break;
                } else {
                    i = stack.pop() + 1;
                }
            } else {
                stack.push(index);
                if (stack.size() == n) {
                    List<T> element = new ArrayList<>();
                    for (Integer x : stack) {
                        element.add(source.get(x));
                    }
                    if (apply.accept(element)) {
                        ++count;
                    }
                    i = index + 1;
                } else {
                    i = 0;
                }
            }
        }

        return count;
    }

    private static Integer findIndexInStack(Stack<Integer> stack, int begin, int end) {
        for (int i = begin; i < end; ++i) {
            if (!stack.contains(i)) {
                return i;
            }
        }

        return null;
    }

}
