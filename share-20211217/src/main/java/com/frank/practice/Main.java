package com.frank.practice;

import com.frank.practice.sample.QueensMap;
import com.frank.practice.template.Algorithm;
import com.frank.practice.template.Utils;

import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        testSort();
        // testPermutation();

    }

    private static void testSort() {
        String[] strArray = {"Frank", "Ethan", "Emma", "Chris", "Icy", "Cheer"};
        Algorithm.sort(strArray, (x, y)->{
            if (x.length() == y.length()) {
                return x.compareTo(y) < 0;
            } else {
                return x.length() < y.length();
            }
        });
        Utils.showArray(strArray);

        Integer[] a = {4, 2, 5, 9, 7, 3, 1, 6, 0, 8};
        Algorithm.sort(a, (x, y)->{ return x > y; });
        Utils.showArray(a);
    }

    private static void testPermutation() {
        List<Integer> source = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8);
        int count = Algorithm.permutation(source, QueensMap::acceptMap);
        System.out.println("count: " + count);

    }


}
