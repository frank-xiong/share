package com.frank.practice.sample;

import java.util.List;

public class QueensMap {

    // 打印一张棋盘
    public static void showMap(List<Integer> map) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < map.size(); ++i) {
            for (int j = 0; j < map.size(); ++j) {
                sb.append(j+1 == map.get(i) ? " *" : " -");
            }
            sb.append('\n');
        }
        System.out.print(sb);
        System.out.println("-----------------");
    }

    // 判定棋盘上指定位置的棋子是否有效
    private static boolean isValid(List<Integer> map, int i) {
        int m = map.get(i);
        for (int j = 0; j < i; ++j) {
            int n = map.get(j);
            if (i - j == m - n || i - j == n - m) {
                return false;
            }
        }

        return true;
    }

    // 判定整张棋盘是否有效
    public static boolean isValid(List<Integer> map) {
        for (int i = 1; i < map.size(); ++i) {
            if (!isValid(map, i)) {
                return false;
            }
        }

        return true;
    }

    public static boolean acceptMap(List<Integer> map) {
        if (QueensMap.isValid(map)) {
            QueensMap.showMap(map);
            return true;
        }

        return false;
    }

}
